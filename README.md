#Teste Banco Inter#

#Funcionamento#

Clone o repositorio para sua maquina e importe ele como maven project em 
qualquer IDE.

Use o comando `mvn clean install` para baixar as dependencias e rodar os testes
unitarios. Ou use o `Run as` e escolha Maven install em sua IDE.

Para rodar a aplicação vá a classe `DigitoUnicoApplication` e click em 
`Run as Java Application`.

Com a `collection`, importe-a no Postman e rode as requisições uma a uma para
ver o fluxo e teste de integração.

Para acessar a documentação criada pelo swagger acesse:
http://localhost:8080/swagger-ui.html

