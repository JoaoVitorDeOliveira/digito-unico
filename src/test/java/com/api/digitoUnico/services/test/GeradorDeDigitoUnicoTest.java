package com.api.digitoUnico.services.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.api.digitoUnico.services.GeradorDeDigitoUnico;


@RunWith(SpringRunner.class)
@WebMvcTest(GeradorDeDigitoUnico.class)
public class GeradorDeDigitoUnicoTest {
	
	private final static String N_ACIMA_DO_MAXIMO = "10000000000000000000000000000000000000000000";
	private final static int K_ACIMA_DO_MAXIMO = 1000001;
	
	//Instancia para testes
	GeradorDeDigitoUnico geradorDeDigitoUnico= new GeradorDeDigitoUnico();
	
	@Test
	public void parametrosComValoresValidos_returnaDigitoUnico() {
	  int result = geradorDeDigitoUnico.digitoUnico("9875", 4);
	  assertEquals(result, 8);
	}
	
	@Test
	public void parametrosComValoresValidos_returnaTrue() {
	  boolean result = geradorDeDigitoUnico.validarParametrosDigitoUnico("9875", 4);
	  assertEquals(result, true);
	}
	
	@Test
	public void parametrosComValorNInvalido_returnaFalse() {
	  boolean result = geradorDeDigitoUnico.validarParametrosDigitoUnico("9875", 0);
	  assertEquals(result, false);
	}
	
	@Test
	public void valorDeN_AcimaDoMaximo_returnaFalse() {
	  boolean result = geradorDeDigitoUnico.validarParametrosDigitoUnico(N_ACIMA_DO_MAXIMO, 4);
	  assertEquals(result, false);
	}
	
	@Test
	public void valorDeK_AcimaDoMaximo_returnaFalse() {
	  boolean result = geradorDeDigitoUnico.validarParametrosDigitoUnico("9875", K_ACIMA_DO_MAXIMO);
	  assertEquals(result, false);
	}
	
	@Test
	public void parametrosComValorKInvalido_returnaFalse() {
	  boolean result = geradorDeDigitoUnico.validarParametrosDigitoUnico("0", 2);
	  assertEquals(result, false);
	}

}
