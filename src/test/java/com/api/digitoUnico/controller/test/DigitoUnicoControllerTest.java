package com.api.digitoUnico.controller.test;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.api.digitoUnico.controller.DigitoUnicoController;
import com.api.digitoUnico.entidades.DigitoUnicoEntidade;
import com.api.digitoUnico.entidades.UsuarioEntidade;
import com.api.digitoUnico.repository.DigitoUnicoRepository;
import com.api.digitoUnico.repository.UsuarioRepository;
import com.api.digitoUnico.services.Cache;
import com.api.digitoUnico.services.GeradorDeDigitoUnico;


@RunWith(SpringRunner.class)
@WebMvcTest(DigitoUnicoController.class)
public class DigitoUnicoControllerTest {
	
	  @Autowired
	  private MockMvc mockMvc;
	  
	  @MockBean
	  private UsuarioRepository usuarioRepository;
	  
	  @MockBean
	  private DigitoUnicoRepository digitoUnicoRepository;
	  
	  @MockBean
	  private GeradorDeDigitoUnico geradorDeDigitoUnico;
	  
	  @MockBean
	  private Cache cache;
	  
	  private UsuarioEntidade usuarioMock = new UsuarioEntidade(1, "Joao Vitor", "joao.vitor@gmail.com", new ArrayList<DigitoUnicoEntidade>());
	  private DigitoUnicoEntidade digUnicoMock = new DigitoUnicoEntidade(1, "9875", 4, 8);
	  private String digUnicoPayload = "{\"n\":\"9875\",\"k\":\"4\"}";
	  
	  @Test
	  public void gerarDigitoUnico_retornaOk() throws Exception {
		  
		  when(geradorDeDigitoUnico.validarParametrosDigitoUnico(Mockito.anyString(), Mockito.anyInt())).thenReturn(true);
		  when(cache.buscarDigitoUnicoDaCache(Mockito.anyString(), Mockito.anyInt())).thenReturn(Optional.empty());
		  
		  mockMvc.perform(post("/digitoUnico").content(digUnicoPayload).contentType(MediaType.APPLICATION_JSON))
	        .andExpect(status().isOk());
	  	}
	  
	  @Test
	  public void gerarDigitoUnicoComParametrosInvalidso_retornaBadRequest() throws Exception {
		  
		  when(geradorDeDigitoUnico.validarParametrosDigitoUnico(Mockito.anyString(), Mockito.anyInt())).thenReturn(false);
		  
		  mockMvc.perform(post("/digitoUnico").content(digUnicoPayload).contentType(MediaType.APPLICATION_JSON))
	        .andExpect(status().isBadRequest());
	  	}
	  
	  @Test
	  public void gerarDigitoUnicoComValorEmCache_retornaOk() throws Exception {
		  
		  when(geradorDeDigitoUnico.validarParametrosDigitoUnico(Mockito.anyString(), Mockito.anyInt())).thenReturn(true);
		  when(cache.buscarDigitoUnicoDaCache(Mockito.anyString(), Mockito.anyInt())).thenReturn(Optional.of(digUnicoMock));
		  
		  mockMvc.perform(post("/digitoUnico").content(digUnicoPayload).contentType(MediaType.APPLICATION_JSON))
	        .andExpect(status().isOk());
	  	}

	  @Test
	  public void associarDigitoUnicoComUsuario_retornaOk() throws Exception {

	      when(usuarioRepository.findById(1)).thenReturn(Optional.of(usuarioMock));
	      when(digitoUnicoRepository.findById(1)).thenReturn(Optional.of(digUnicoMock));

	      mockMvc.perform(put("/digitoUnico/1/associarUsuario/1").contentType(MediaType.APPLICATION_JSON))
	        .andExpect(status().isOk());
	  }
	  
	  @Test
	  public void associarDigitoUnicoComUsuarioInvalido_retornaUsuarioNaoEncontrado() throws Exception {

	      when(usuarioRepository.findById(0)).thenReturn(Optional.empty());

	      mockMvc.perform(put("/digitoUnico/1/associarUsuario/0").contentType(MediaType.APPLICATION_JSON))
	        .andExpect(status().isNotFound());
	  }
	  
	  @Test
	  public void associarDigitoUnicoInvalidoComUsuario_retornaDigitoNaoEncontrado() throws Exception {

		  when(usuarioRepository.findById(1)).thenReturn(Optional.of(usuarioMock));
	      when(digitoUnicoRepository.findById(0)).thenReturn(Optional.empty());

	      mockMvc.perform(put("/digitoUnico/0/associarUsuario/1").contentType(MediaType.APPLICATION_JSON))
	        .andExpect(status().isNotFound());
	  }
	  
	  @Test
	  public void associarDigitoUnicoComUsuarioJaAssociado_retornaOk() throws Exception {
		  
		  usuarioMock.setDigitoUnicoResultado(Arrays.asList(digUnicoMock));

	      when(usuarioRepository.findById(1)).thenReturn(Optional.of(usuarioMock));
	      when(digitoUnicoRepository.findById(1)).thenReturn(Optional.of(digUnicoMock));

	      mockMvc.perform(put("/digitoUnico/1/associarUsuario/1").contentType(MediaType.APPLICATION_JSON))
	        .andExpect(status().isNoContent());
	  }
	  
	  @Test
	  public void buscarDigitoUnicoPorIdUsuario_retornaOk() throws Exception {
		  
		  usuarioMock.setDigitoUnicoResultado(Arrays.asList(digUnicoMock));

	      when(usuarioRepository.findById(1)).thenReturn(Optional.of(usuarioMock));

	      mockMvc.perform(get("/digitoUnico/usuario/" + 1).contentType(MediaType.APPLICATION_JSON))
	          .andExpect(status().isOk())
	          .andExpect(jsonPath("$", hasSize(1)))
	          .andExpect(jsonPath("$[0].digitoUnico", is(digUnicoMock.getDigitoUnico())));
	  }

}
