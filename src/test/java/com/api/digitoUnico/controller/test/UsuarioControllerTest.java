package com.api.digitoUnico.controller.test;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.api.digitoUnico.controller.UsuarioController;
import com.api.digitoUnico.entidades.UsuarioEntidade;
import com.api.digitoUnico.repository.UsuarioRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(UsuarioController.class)
public class UsuarioControllerTest {
	
	  @Autowired
	  private MockMvc mockMvc;

	  @MockBean
	  private UsuarioRepository usuarioRepository;

	  private UsuarioEntidade usuarioMock = new UsuarioEntidade(1, "Joao Vitor", "joao.vitor@gmail.com");
	  
	  @Test
	  public void criarUsuario_retornaCreated() throws Exception {

	    when(usuarioRepository.save(Mockito.any())).thenReturn(usuarioMock);

	    mockMvc.perform(post("/usuario").content("{\"name\":\"Joao Vitor\",\"email\":\"joao.vitor@gmail.com\"}").contentType(MediaType.APPLICATION_JSON))
	        .andExpect(status().isCreated())
	        .andExpect(jsonPath("$.nome", is("Joao Vitor")));
	  }
	  
	  @Test
	  public void atualizarUsuario_retornaOk() throws Exception {

	    when(usuarioRepository.findById(1)).thenReturn(Optional.of(usuarioMock));
	    //when(usuarioRepository.save(Mockito.any())).thenReturn(usuarioMock);

	    mockMvc.perform(put("/usuario/" + 1).content("{\"nome\":\"Vitor oliveira\",\"email\":\"vitor.oliveira@gmail.com\"}").contentType(MediaType.APPLICATION_JSON))
	        .andExpect(status().isOk());	       
	  }
	  
	  @Test
	  public void atualizarUsuarioComIdInvalido_retornaUsuarioNaoEncontrado() throws Exception {

	    when(usuarioRepository.findById(0)).thenReturn(Optional.empty());

	    mockMvc.perform(put("/usuario/" + 0).content("{\"nome\":\"Vitor oliveira\",\"email\":\"vitor.oliveira@gmail.com\"}").contentType(MediaType.APPLICATION_JSON))
	        .andExpect(status().isNotFound());
	       
	  }

	  @Test
	  public void buscarTodosUsuario_retornaListaUsuarios() throws Exception {
		  
	    List<UsuarioEntidade> listaUsuarios = new ArrayList<UsuarioEntidade>();
	    listaUsuarios.add(usuarioMock);

	    when(usuarioRepository.findAll()).thenReturn(listaUsuarios);

	    mockMvc.perform(get("/usuarios").contentType(MediaType.APPLICATION_JSON))
	        .andExpect(status().isOk())
	        .andExpect(jsonPath("$", hasSize(1)))
	        .andExpect(jsonPath("$[0].nome", is(usuarioMock.getNome())));
	  }
	  
	  @Test
	  public void buscarUsuarioPeloId_retornaUsuario() throws Exception {

		when(usuarioRepository.findById(1)).thenReturn(Optional.of(usuarioMock));

	    mockMvc.perform(get("/usuario/" + 1).contentType(MediaType.APPLICATION_JSON))
	        .andExpect(status().isOk())
	        .andExpect(jsonPath("$.nome", is(usuarioMock.getNome())));
	  }
	  
	  @Test
	  public void buscarUsuarioComIdInvalido_retornaUsuarioNaoEncontrado() throws Exception {

		when(usuarioRepository.findById(0)).thenReturn(Optional.empty());

	    mockMvc.perform(get("/usuario/" + 1).contentType(MediaType.APPLICATION_JSON))
	        .andExpect(status().isNotFound());
	  }
	  
	  @Test
	  public void deletarUsuario_retornaOk() throws Exception {

	    mockMvc.perform( delete("/usuario/" + 1).contentType(MediaType.APPLICATION_JSON))
	        .andExpect(status().isOk());
	  }
}
