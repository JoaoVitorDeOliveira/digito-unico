package com.api.digitoUnico.controller.test;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.api.digitoUnico.controller.CriptografiaController;
import com.api.digitoUnico.entidades.UsuarioEntidade;
import com.api.digitoUnico.repository.UsuarioRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(CriptografiaController.class)
public class CriptografiaControllerTest {

	  @Autowired
	  private MockMvc mockMvc;
	  
	  @MockBean
	  private UsuarioRepository usuarioRepository;
	  	  
	  private UsuarioEntidade usuarioMock = new UsuarioEntidade(1, "Joao Vitor", "joao.vitor@gmail.com");
	  
	  @Test
	  public void criptografarDados_retornaOk() throws Exception {
		  
		  when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(usuarioMock));
		  
		  List<String> list = chavesPublicaEPrivada();
		  mockMvc.perform(put("/criptografarUsuario/1").content("{\"chavePublica\":\"" + list.get(1) + "\"}").contentType(MediaType.APPLICATION_JSON))
	        .andExpect(status().isOk());
	  	}
	  
	  
	  private List<String> chavesPublicaEPrivada() throws NoSuchAlgorithmException{
		  
		    KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
		    keyPairGenerator.initialize(2048);
		    KeyPair keyPair = keyPairGenerator.generateKeyPair();
		    String b64PublicKey = Base64.getEncoder().encodeToString(keyPair.getPublic().getEncoded());
		    String b64PrivateKey = Base64.getEncoder().encodeToString(keyPair.getPrivate().getEncoded());
			return Arrays.asList(b64PrivateKey, b64PublicKey);
	  }
}
