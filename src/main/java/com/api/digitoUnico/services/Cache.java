package com.api.digitoUnico.services;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.api.digitoUnico.entidades.DigitoUnicoEntidade;

@Service
public class Cache {
	
	private static Set<DigitoUnicoEntidade> cacheDigitosUnicos = new LinkedHashSet<DigitoUnicoEntidade>();
	
	/**
	 * Adiciona um novo Digito Unico a Cache
	 * @param digitoUnicoEntidade
	 */
	public void adicionarDigitoAoCache(DigitoUnicoEntidade digitoUnicoEntidade) {
		
		cacheDigitosUnicos.add(digitoUnicoEntidade);
		
		if(cacheDigitosUnicos.size() > 10) {
			
			List<DigitoUnicoEntidade> auxLista = new ArrayList<DigitoUnicoEntidade>();
			
			auxLista = cacheDigitosUnicos.stream().skip(1).collect(Collectors.toList());
			cacheDigitosUnicos.clear();
			cacheDigitosUnicos.addAll(auxLista);
			auxLista.clear();
		}	
		
	}
	
	/**
	 * Faz a busca na cache
	 * @param n
	 * @param k
	 * @return se digito esta presente ou nao
	 */
	public Optional<DigitoUnicoEntidade> buscarDigitoUnicoDaCache(String n, int k) {
		
		Optional<DigitoUnicoEntidade> digitoDaCache = cacheDigitosUnicos.stream()
													.filter(digitoUnico -> digitoUnico.getN().equals(n) && digitoUnico.getK() == k)
													.findFirst();	
		if(digitoDaCache.isPresent()) {
			return digitoDaCache;
		}
		
		return digitoDaCache;
	}

}
