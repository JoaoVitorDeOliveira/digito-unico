package com.api.digitoUnico.services;

import java.math.BigInteger;
import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class GeradorDeDigitoUnico {
	
	private final static double VALOR_MAXIMO_N =  Math.pow(10, 1000000);
	private final static double VALOR_MAXIMO_K = Math.pow(10, 5);
	
	private static Logger logger = LoggerFactory.getLogger(GeradorDeDigitoUnico.class);
	
	/**
	 * Gerador de Digito Unico
	 * @param n uma string representado um inteiro. 1<=n<=10ˆ1000000
	 * @param k um inteiro representando o número de vezes da concatenação 1<=k<=10ˆ5
	 * @return int com a soma dos digitos unicos
	 */
	  public int digitoUnico(String n, int k) {
		  
		  int n_value = new BigInteger(n).intValue();
		  	    
		  logger.info("Parametros aceitos: N " + n_value + ", K " + k);
	      if(n.length() == 1 && k == 1) {
	    	  logger.info("Resultado com sucesso: " + n);
	    	  return n_value;
	      }
	    
	      IntStream chars = n.chars().map(Character::getNumericValue);;
	    
	      String sum = String.valueOf(chars.sum() * k);
	    
	      return digitoUnico(sum, 1);	    
	  }

	
	/**
	 * Valida os parametros de acordo com a regra abaixo
	 * @param n uma string representado um inteiro. 1<=n<=10ˆ1000000
	 * @param k um inteiro representando o número de vezes da concatenação 1<=k<=10ˆ5
	 * @return boolean
	 */
	public boolean validarParametrosDigitoUnico(String n, int k) {
		
		int n_value = new BigInteger(n).intValue();
		
		if( n_value < 1 || n_value > VALOR_MAXIMO_N) {
			  logger.info("N com valor invalido " + n_value);
			  return false;
		  } 
		  
		else if(k < 1 || k > VALOR_MAXIMO_K) {
			  logger.info("K com valor invalido " + k);
			  return false;
		 }
		
		return true;
	} 	  

}
