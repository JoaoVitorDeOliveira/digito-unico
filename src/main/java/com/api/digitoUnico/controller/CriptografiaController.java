package com.api.digitoUnico.controller;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Map;
import java.util.Optional;

import javax.crypto.Cipher;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.api.digitoUnico.entidades.UsuarioEntidade;
import com.api.digitoUnico.repository.UsuarioRepository;

@RestController
public class CriptografiaController {
	
	private static final String ALGORITMO_CRIPTOGAFIA = "RSA";
	private static final String ENCODE = "UTF-8";
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	/**
	 * EndPoint para criptografa os dados de um Usuario com uma chave publica gerada
	 * @param id
	 * @return Dados criptografados e chave privada salva no Usuario
	 * @throws IOException
	 * @throws GeneralSecurityException
	 */
	  @PutMapping("/criptografarUsuario/{id}")
	  public ResponseEntity<Object> criptografarUsuario(@RequestBody Map<String, String> payload, @PathVariable Integer id) throws IOException, GeneralSecurityException {
		  
	    Optional<UsuarioEntidade> usuarioBuscado = usuarioRepository.findById(id);

	    if (!usuarioBuscado.isPresent()) {
	        return ResponseEntity.notFound().build();
	    }
	    
	    UsuarioEntidade usuario = usuarioBuscado.get();

	    PublicKey chavePublica = recebendoChavePublica(payload.get("chavePublica"));
	    
	    usuario.setNome(criptografarTexto(usuario.getNome(), chavePublica));
	    usuario.setEmail(criptografarTexto(usuario.getEmail(), chavePublica));
	    
	    return ResponseEntity.ok(usuarioRepository.save(usuario));


	  }
	  
	  /**
	   * EndPoint para descriptografar os dados de um Usuario usando sua chave privada salva
	   * @param id
	   * @return Dados descriptografados
	   * @throws IOException
	   * @throws GeneralSecurityException
	   */
	  @PutMapping("/descriptografarUsuario/{id}")
	  public ResponseEntity<Object> descriptografarUsuario(@RequestBody Map<String, String> payload, @PathVariable Integer id) throws IOException, GeneralSecurityException {
		  
	    Optional<UsuarioEntidade> usuarioBuscado = usuarioRepository.findById(id);

	    if (!usuarioBuscado.isPresent()) {
	        return ResponseEntity.notFound().build();
	    }
	    
	    UsuarioEntidade usuario = usuarioBuscado.get(); 
	    
	    PrivateKey chavePrivada = recebendoChavePrivada(payload.get("chavePrivada"));
	    
    	usuario.setNome(descriptografarTexto(usuario.getNome(), chavePrivada));
    	usuario.setEmail(descriptografarTexto(usuario.getEmail(), chavePrivada));
    	
    	return ResponseEntity.ok(usuarioRepository.save(usuario));
	        

	  }
	  
	  private String criptografarTexto(String texto, PublicKey chavePublica) throws IOException, GeneralSecurityException {
		    Cipher cipher = Cipher.getInstance(ALGORITMO_CRIPTOGAFIA);
		    cipher.init(Cipher.ENCRYPT_MODE, chavePublica);
		    return Base64.encodeBase64String(cipher.doFinal(texto.getBytes(ENCODE)));
	  }
	  
	  private String descriptografarTexto(String txtCriptografado, PrivateKey chavePrivada) throws IOException, GeneralSecurityException {
		    Cipher cipher = Cipher.getInstance(ALGORITMO_CRIPTOGAFIA);
		    cipher.init(Cipher.DECRYPT_MODE, chavePrivada);
		    return new String(cipher.doFinal(Base64.decodeBase64(txtCriptografado)), ENCODE);
	  }
	  
	  private PublicKey recebendoChavePublica(String chave) throws IOException, GeneralSecurityException {
		    KeyFactory geradorChave = KeyFactory.getInstance(ALGORITMO_CRIPTOGAFIA);
		    PublicKey chavePublica = geradorChave.generatePublic(new X509EncodedKeySpec(Base64.decodeBase64(chave)));
		    return chavePublica;
	  }
	  
	  private PrivateKey recebendoChavePrivada(String chave) throws IOException, GeneralSecurityException {
		    KeyFactory geradorChave = KeyFactory.getInstance(ALGORITMO_CRIPTOGAFIA);
		    PrivateKey chavePrivada = geradorChave.generatePrivate(new PKCS8EncodedKeySpec(Base64.decodeBase64(chave)));
		    return chavePrivada;
	  }

}
