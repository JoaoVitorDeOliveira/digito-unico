package com.api.digitoUnico.controller;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.api.digitoUnico.entidades.DigitoUnicoEntidade;
import com.api.digitoUnico.entidades.UsuarioEntidade;
import com.api.digitoUnico.repository.DigitoUnicoRepository;
import com.api.digitoUnico.repository.UsuarioRepository;
import com.api.digitoUnico.services.Cache;
import com.api.digitoUnico.services.GeradorDeDigitoUnico;

@RestController
public class DigitoUnicoController {
	
	private static Logger logger = LoggerFactory.getLogger(DigitoUnicoController.class);
	
	@Autowired
	private DigitoUnicoRepository digitoUnicoRepository;
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private GeradorDeDigitoUnico geradorDeDigitoUnico;
	
	@Autowired
	private Cache cache;
	
	  /**
	   * EndPoint para gerar o Digito Unico.
	   *
	   * @param digitoUnico  recebendo N e K como parametro
	   * @return Entidade com o digito unico gerado
	   */
	  @PostMapping("/digitoUnico")
	  public ResponseEntity<DigitoUnicoEntidade> gerarDigitoUnico(@RequestBody DigitoUnicoEntidade digitoUnico) {
		  
		String n = digitoUnico.getN();
		int k = digitoUnico.getK();
		  
		if(!geradorDeDigitoUnico.validarParametrosDigitoUnico(n, k)) {
			return ResponseEntity.badRequest().build();
		}
		  
	    Optional<DigitoUnicoEntidade> buscarDaCache = cache.buscarDigitoUnicoDaCache(n, k);
	    if(buscarDaCache.isPresent()) {
	    	return ResponseEntity.ok(buscarDaCache.get());
	    }
	    
	    digitoUnico.setDigitoUnico(geradorDeDigitoUnico.digitoUnico(n, k));
		cache.adicionarDigitoAoCache(digitoUnico);
	    
	    return ResponseEntity.ok(digitoUnicoRepository.save(digitoUnico));
	  }
	  
	  /**
	   * EndPoint parar Associar digitos unicos a usuarios.
	   *
	   * @param idDigitoUnico id do Digito Unico a ser asscoiado
	   * @param idUsuario id do usuario a ser associado
	   * @return Usuario associado com o Digito Unico
	   */
	  @PutMapping("/digitoUnico/{idDigitoUnico}/associarUsuario/{idUsuario}")
	  public ResponseEntity<UsuarioEntidade> associarDigitoUnicoComUsuario(@PathVariable int idDigitoUnico, @PathVariable int idUsuario) {		
		
	    Optional<UsuarioEntidade> usuario = usuarioRepository.findById(idUsuario);

	    if (!usuario.isPresent()) {
	      return ResponseEntity.notFound().build();
	    }

	    Optional<DigitoUnicoEntidade> digitoUnicoParaAssociar = digitoUnicoRepository.findById(idDigitoUnico);

	    if (!digitoUnicoParaAssociar.isPresent()) {
	      return ResponseEntity.notFound().build();
	    }

	    List<DigitoUnicoEntidade> resultadosDigitosUnicos = usuario.get().getDigitoUnicoResultado(); 
	    
	    for(DigitoUnicoEntidade due: resultadosDigitosUnicos) {
	    	if(due.getId() == idDigitoUnico) {
	    		 logger.info("Esse Digito Unico já está associado a esse Usuário");
	    		 return ResponseEntity.noContent().build();
	    	}
	    }
	    
		resultadosDigitosUnicos.add(digitoUnicoParaAssociar.get());
	      
	    return ResponseEntity.ok(usuarioRepository.save(usuario.get()));

	  }
	  
	  /**
	   * Buscar digitos unicos associados com Usuarios
	   *
	   * @param id do Usuario
	   * @return Lista de Digitos Unicos
	   */
	  @GetMapping("/digitoUnico/usuario/{id}")
	  public List<DigitoUnicoEntidade> buscarDigitoUnicoPorUsuario(@PathVariable int id) {
	    Optional<UsuarioEntidade> usuario = usuarioRepository.findById(id);

	    return usuario.get().getDigitoUnicoResultado();
	  }

}
