package com.api.digitoUnico.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.api.digitoUnico.entidades.UsuarioEntidade;
import com.api.digitoUnico.repository.UsuarioRepository;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * EndPoints para o CRUD de Usuarios
 * 
 * @author Joao
 *
 */
@RestController
public class UsuarioController {
	
	  private static Logger logger = LoggerFactory.getLogger(UsuarioController.class);
	  
	  @Autowired
	  private UsuarioRepository usuarioRepository;	  
	  

	  
	  /**
	   * Busca Todos os Usuarios cadastrados
	   * 
	   * @return lista de Usuarios cadastrados
	   */
	  @GetMapping("/usuarios")
	  @ApiOperation(
				value="Cadastrar um novo usuario", 
				response=UsuarioEntidade.class, 
				notes="Essa operação salva um novo registro com as informações de usuario.")
		@ApiResponses(value= {
				@ApiResponse(
						code=200, 
						message="Retorna um UsuarioEntidade com uma mensagem de sucesso",
						response=UsuarioEntidade.class
						),
				@ApiResponse(
						code=500, 
						message="Caso tenhamos algum erro vamos retornar um UsuarioEntidade com a Exception",
						response=UsuarioEntidade.class
						)
	 
		})
	  public List<UsuarioEntidade> buscarUsuarios(){
		
		logger.info("Criando lista de Usuarios...");
		List<UsuarioEntidade> usuarioEntidades = new ArrayList<UsuarioEntidade>();
		
		logger.info("Buscando e Populando lista");
		usuarioRepository.findAll().forEach(cadaUsuario -> usuarioEntidades.add(cadaUsuario));
		
		logger.info("Resultado", usuarioEntidades);
		return usuarioEntidades;  
		  
	  }
	  
	  /**
	   * Busca Usuarios cadastrados por ID
	   *
	   * @param id Usuario
	   * @return Usuario com id
	   */
	  @GetMapping("/usuario/{id}")
	  public ResponseEntity<UsuarioEntidade> buscarUsuarioPorId(@PathVariable int id) {
		logger.info("Buscando Usuario...");
	    Optional<UsuarioEntidade> usuario = usuarioRepository.findById(id);

	    if (!usuario.isPresent()) {
	      logger.info("Usuario nao encontrado ou nao existe!");
	      return ResponseEntity.notFound().build();
	    }
	    logger.info("Usuario encontrado!");
	    return ResponseEntity.ok(usuario.get());
	  }
	  
	  /**
	   * Criar um novo Usuario
	   * 
	   * @param usuario
	   * @return novo Usuario
	   */
	  @PostMapping("/usuario")
	  public ResponseEntity<UsuarioEntidade> criarUsuario(@RequestBody UsuarioEntidade usuario) {
		  
		logger.info("Criando um novo usuario...");
	    UsuarioEntidade novoUsuario = usuarioRepository.save(usuario);
	    
	    
	    logger.info("Usuario Criado");
	    return ResponseEntity.created(URI.create("/usuario/" + novoUsuario.getId())).body(novoUsuario);
	  }
	  
	  /**
	   * Atualizar Usuario.
	   *
	   * @param usuario 
	   * @param id 
	   * @return Status Ok
	   */
	  @PutMapping("/usuario/{id}")
	  public ResponseEntity<UsuarioEntidade> atualizarUsuario(@RequestBody UsuarioEntidade usuario, @PathVariable int id) {
		logger.info("Buscando Usuario...");
	    Optional<UsuarioEntidade> usuarioEncontrado = usuarioRepository.findById(id);

	    if (!usuarioEncontrado.isPresent()) {
	      logger.info("Usuario nao encontrado ou nao existe!");
	      return ResponseEntity.notFound().build();
	    }

	    logger.info("Usuario encontrado!");
	    usuario.setId(id);

	    logger.info("Usuario Atualizado!");
	    return ResponseEntity.ok(usuarioRepository.save(usuario));
	  }

	  /**
	   * Deletar Usuario por Id.
	   *
	   * @param id do Usuario
	   */
	  @DeleteMapping("/usuario/{id}")
	  public void deletarUsuario(@PathVariable int id) {
		logger.info("Deletando Usuario...");
	    usuarioRepository.deleteById(id);
	  }
	  
}
