package com.api.digitoUnico.entidades;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


/**
 * Entidade representando o DigitoUnico
 * 
 * @author Joao
 */
@Entity
public class DigitoUnicoEntidade {
	
	@Id
	@GeneratedValue
	private int id;
	
	private String n;

	private int k;
	private int digitoUnico;
  	  
	public DigitoUnicoEntidade(int id, String n, int k, int digitoUnico) {
		super();
		this.id = id;
		this.n = n;
		this.k = k;
		this.digitoUnico = digitoUnico;
	}
	
	public DigitoUnicoEntidade() {
		super();
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getN() {
		return n;
	}
	
	public void setN(String n) {
		this.n = n;
	}
	
	public int getK() {
		return k;
	}
	
	public void setK(int k) {
		this.k = k;
	}
	
	public int getDigitoUnico() {
		return digitoUnico;
	}
	
	public void setDigitoUnico(int digitoUnico) {
		this.digitoUnico = digitoUnico;
	}
	
/*	@Override
		public String toString() {
			return "" + this.getN() + "," + this.getK() + "," + this.getDigitoUnico() + "";
		}*/

}
