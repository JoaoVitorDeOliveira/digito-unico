package com.api.digitoUnico.entidades;

import java.security.PrivateKey;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * Entidade representado um Usuário real
 * 
 * 
 * @author Joao
 */
@Entity
public class UsuarioEntidade {
	
	@Id
	@GeneratedValue
	private int id;
	
	@Column
	@Lob
	private String nome;
	
	@Column
	@Lob
	private String email;
	
	@OneToMany(cascade = CascadeType.ALL)
	private List<DigitoUnicoEntidade> digitoUnicoResultado;

	public UsuarioEntidade(int id, String nome, String email, List<DigitoUnicoEntidade> digitoUnicoResultado) {
		super();
		this.id = id;
		this.nome = nome;
		this.email = email;
		this.digitoUnicoResultado = digitoUnicoResultado;
	}

	public UsuarioEntidade(int id, String nome, String email) {
		super();
		this.id = id;
		this.nome = nome;
		this.email = email;
	}
	
	public UsuarioEntidade() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<DigitoUnicoEntidade> getDigitoUnicoResultado() {
		return digitoUnicoResultado;
	}

	public void setDigitoUnicoResultado(List<DigitoUnicoEntidade> digitoUnicoResultado) {
		this.digitoUnicoResultado = digitoUnicoResultado;
	}
	
}
